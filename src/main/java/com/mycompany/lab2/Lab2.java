/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author chanatip
 */
public class Lab2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    static void printWelcome() {
        System.out.println("WELCOME TO OX GAME");
    }

    static void printTable() {
        System.out.println("---------");
        for (int i = 0; i < 3; i++) {
            System.out.print("|");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + "|");
            }
            System.out.println("\n---------");
        }
    }

    static void printTurnANDinputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Turn " + currentPlayer + ", enter your move (row col): ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
        }

    }

    private static void switchPlayer() {
        currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
    }

    private static void PrintWin() {
        System.out.println("Player:" + currentPlayer + " WIN!!!");
    }

    private static boolean isWin() {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == currentPlayer && table[i][1] == currentPlayer && table[i][2] == currentPlayer) {
                return true;
            }

            if (table[0][i] == currentPlayer && table[1][i] == currentPlayer && table[2][i] == currentPlayer) {
                return true;
            }
        }

        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }

        return false;
    }

    private static boolean isTableFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurnANDinputRowCol();

            if (isWin()) {
                printTable();
                System.out.println();
                PrintWin();
                break;
            }

            if (isTableFull()) {
                printTable();
                System.out.println("It's a tie!");
                break;
            }

            switchPlayer();
        }
    }

}
